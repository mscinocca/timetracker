import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DayEditorComponent } from './components/day-editor/day-editor.component';

const routes: Routes = [
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'dashboard', component: DashboardComponent, data: { Name: "Timesheets"} },
    { path: 'dayeditor/:timesheetId/:timesheetDayId', component: DayEditorComponent, data: { Name: "Day Editor"}},
  ];
  
  @NgModule({
    imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
    exports: [ RouterModule ]
  })
  
  export class AppRoutingModule {}