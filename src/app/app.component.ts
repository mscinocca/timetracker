import { Component } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TimeTracker';

  currentRouteName: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute) { }
  
  ngOnInit() {
    this.router.events
    .subscribe(event => {
      
      let currentRoute = this.route.root;

      while (currentRoute.children[0] !== undefined) {
        currentRoute = currentRoute.children[0];
      }

      this.currentRouteName = currentRoute.snapshot.data.Name;
    })
  }
}
