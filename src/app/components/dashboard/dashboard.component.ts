import { Component, OnInit } from '@angular/core';
import { TimesheetService } from '../../service/timesheet.service';
import { Timesheet } from '../../model/timesheet';
import { TimesheetDay } from '../../model/timesheet-day';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  todayDate: Date;

  timesheets: Array<Timesheet>;

  constructor(
    private timesheetService: TimesheetService,
    private router: Router,
    ) { }

  ngOnInit() {
    this.todayDate = new Date(2018, 8, 12);
    
    this.timesheets = this.timesheetService.getTimesheets();
  }

  EditTimesheetDay(timesheet: Timesheet, timesheetDay: TimesheetDay): void {
    this.router.navigate(['dayeditor', timesheet.Id, timesheetDay.Id]);
  }

  EditTimesheet(timesheet: Timesheet): void {

  }
}
