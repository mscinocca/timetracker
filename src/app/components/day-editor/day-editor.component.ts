import { Component, OnInit } from '@angular/core';
import { TimesheetDay } from '../../model/timesheet-day';
import { TimesheetService } from '../../service/timesheet.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Timesheet } from '../../model/timesheet';
import { TimeAllocation } from '../../model/time-allocation';
import { TimesheetItem } from '../../model/timesheet-item';

@Component({
  selector: 'app-day-editor',
  templateUrl: './day-editor.component.html',
  styleUrls: ['./day-editor.component.css']
})
export class DayEditorComponent implements OnInit {

  timesheet: Timesheet;
  timesheetDay: TimesheetDay;

  constructor(
    private timesheetService: TimesheetService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params =>
      {
        this.timesheet = this.timesheetService.getTimesheetById(params['timesheetId']);
        this.timesheetDay = this.timesheetService.getTimesheetDay(params['timesheetId'], params['timesheetDayId']);
      });
  }

  SubtractHours(timesheetItem: TimesheetItem) {
    var currentTime = this.timesheet.GetTotalHoursForItemForDay(timesheetItem, this.timesheetDay);
    currentTime -= 0.25;

    this.timesheet.SetTimeAllocationHours(timesheetItem, this.timesheetDay, currentTime, "Worked");
  }

  AddHours(timesheetItem: TimesheetItem) {
    var currentTime = this.timesheet.GetTotalHoursForItemForDay(timesheetItem, this.timesheetDay);
    currentTime += 0.25;

    this.timesheet.SetTimeAllocationHours(timesheetItem, this.timesheetDay, currentTime, "Worked");
  }

  Save() {
    this.router.navigate(['dashboard']);
  }
}
