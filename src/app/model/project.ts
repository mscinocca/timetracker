export class Project {
   Id: number;
   Name: string;

   constructor(id: number, name: string) {
       this.Id = id;
       this.Name = name;
   }
}