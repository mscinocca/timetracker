import { TimesheetItem } from "./timesheet-item";
import { TimesheetDay } from "./timesheet-day";

export class TimeAllocation {
   TimesheetItem: TimesheetItem;
   TimesheetDay: TimesheetDay;

   Time: number;
   Comment: string;

   constructor(timesheetItem: TimesheetItem, timesheetDay: TimesheetDay, time: number, comment: string) {
       this.TimesheetItem = timesheetItem;
       this.TimesheetDay = timesheetDay;

       this.Time = time;
       this.Comment = comment;
   }
}