export class TimesheetDay {
  Id: number;
  Date: Date;

  constructor(id: number, date: Date) {
    this.Id = id;
    this.Date = date;
  }

  public IsToday(): boolean {
    return this.Date.getTime() == new Date(2018, 8, 12).getTime();
  }
}