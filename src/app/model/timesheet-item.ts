import { Project } from "./project";
import { Task } from "./task";
import { TimeAllocation } from "./time-allocation";

export class TimesheetItem {
   Id: number;
   Project: Project;
   Task: Task;

   constructor(id: number, project: Project, task: Task) {
       this.Id = id;
       this.Project = project;
       this.Task = task;
   }
}