import { TimesheetItem } from "./timesheet-item";
import { TimesheetDay } from "./timesheet-day";
import { TimeAllocation } from "./time-allocation";

export enum eTimesheetStatus {
	ToDo = 1,
	Draft = 2,
	Submitted = 3,
	Final = 4,
}

export class Timesheet {
	Id: number;
	StartDate: Date;
	EndDate: Date;
	Status: eTimesheetStatus;

	TimesheetDays: Array<TimesheetDay> = new Array<TimesheetDay>();

	TimesheetItems: Array<TimesheetItem> = new Array<TimesheetItem>();

	TimeAllocation: Array<TimeAllocation> = new Array<TimeAllocation>();

	constructor(id: number, startDate: Date, endDate: Date) {
		this.Id = id;
		this.StartDate = startDate;
		this.EndDate = endDate;

		this.Status = eTimesheetStatus.ToDo;

		for (var i = startDate.getDate(); i <= endDate.getDate(); i++) {
			this.TimesheetDays.push(new TimesheetDay(i, new Date(startDate.getFullYear(), startDate.getMonth(), i)));
		}
	}

	public GetTimesheetWeekDays(): Array<TimesheetDay> {
		return this.TimesheetDays.filter(x => x.Date.getDay() > 0 && x.Date.getDay() < 6);
	}

	public GetTotalHours(): number {
		var totalTime = 0.0;

		this.TimeAllocation.forEach(x => totalTime += x.Time);

		return totalTime;
	}

	public GetTotalHoursForDay(timesheetDay: TimesheetDay): number {
		var totalTime = 0.0;

		this.TimeAllocation.filter(x => x.TimesheetDay == timesheetDay).forEach(x => totalTime += x.Time);

		return totalTime;
	}

	public GetTotalHoursForItem(timesheetItem: TimesheetItem): number {
		var totalTime = 0.0;

		this.TimeAllocation.filter(x => x.TimesheetItem.Id == timesheetItem.Id).forEach(x => totalTime += x.Time);

		return totalTime;
	}

	public GetTotalHoursForItemForDay(timesheetItem: TimesheetItem, timesheetDay: TimesheetDay): number {
		var totalTime = 0.0;

		this.TimeAllocation.filter(x => x.TimesheetItem.Id == timesheetItem.Id && x.TimesheetDay.Id == timesheetDay.Id).forEach(x => totalTime += x.Time);

		return totalTime;
	}

	public GetCommentForItemForDay(timesheetItem: TimesheetItem, timesheetDay: TimesheetDay): string {
		var comment: string = "";

		var timeAllocation = this.TimeAllocation.find(x => x.TimesheetItem.Id == timesheetItem.Id && x.TimesheetDay.Id == timesheetDay.Id);

		if(timeAllocation != null) {
			comment = timeAllocation.Comment;
		}

		return comment;
	}

	public SetTimeAllocationHours(timesheetItem: TimesheetItem, timesheetDay: TimesheetDay, hours: number, comment: string) {
		var timeAllocation = this.TimeAllocation.find(x => x.TimesheetItem.Id == timesheetItem.Id && x.TimesheetDay.Id == timesheetDay.Id);

		if(timeAllocation == null) {
			timeAllocation = new TimeAllocation(timesheetItem, timesheetDay, 0, comment);

			this.TimeAllocation.push(timeAllocation);
		}

		timeAllocation.Time = hours;
	}

	public IsToDo(): boolean {
		return this.Status == eTimesheetStatus.ToDo;
	}

	public IsInProgess(): boolean {
		return this.Status == eTimesheetStatus.Draft;
	}

	public IsComplete(): boolean {
		return this.Status == eTimesheetStatus.Submitted || this.Status == eTimesheetStatus.Final;
	}

	public CanEdit(): boolean {
		return this.Status == eTimesheetStatus.ToDo || this.Status == eTimesheetStatus.Draft;
	}

	public CanSign(): boolean {
		return this.Status == eTimesheetStatus.Draft;
	}

	public CanRecall(): boolean {
		return this.Status == eTimesheetStatus.Submitted;
	}
}