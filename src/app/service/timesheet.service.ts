import { Injectable } from '@angular/core';
import { Timesheet, eTimesheetStatus } from '../model/timesheet';
import { Project } from '../model/project';
import { TimesheetItem } from '../model/timesheet-item';
import { TimeAllocation } from '../model/time-allocation';

@Injectable({
  providedIn: 'root',
})
export class TimesheetService {

  timesheets = new Array<Timesheet>();

  constructor() { }

  getTimesheets(): Array<Timesheet> {

    if(this.timesheets.length == 0) {

      var project1 = new Project(1, "Mackinac");
      var task1 = new Project(1, "Client Meetings");
      var task2 = new Project(1, "Travel");

      var timesheetItem1 = new TimesheetItem(1, project1, task1);
      var timesheetItem2 = new TimesheetItem(2, project1, task2);

      var timesheet1: Timesheet = new Timesheet(1, new Date(2018, 8, 3), new Date(2018, 8, 9));
      timesheet1.Status = eTimesheetStatus.Submitted;
      timesheet1.TimesheetItems.push(timesheetItem1);
      timesheet1.TimesheetItems.push(timesheetItem2);

      timesheet1.TimeAllocation.push(new TimeAllocation(timesheetItem1, timesheet1.TimesheetDays[0], 7.5, "In person meeting"));
      timesheet1.TimeAllocation.push(new TimeAllocation(timesheetItem1, timesheet1.TimesheetDays[1], 7.5, "In person meeting"));
      timesheet1.TimeAllocation.push(new TimeAllocation(timesheetItem1, timesheet1.TimesheetDays[2], 7.5, "In person meeting"));
      timesheet1.TimeAllocation.push(new TimeAllocation(timesheetItem1, timesheet1.TimesheetDays[3], 7.5, "In person meeting"));
      timesheet1.TimeAllocation.push(new TimeAllocation(timesheetItem1, timesheet1.TimesheetDays[4], 7.5, "In person meeting"));

      var timesheet2: Timesheet = new Timesheet(2, new Date(2018, 8, 10), new Date(2018, 8, 16));
      timesheet2.Status = eTimesheetStatus.Draft;
      timesheet2.TimesheetItems.push(timesheetItem1);
      timesheet2.TimesheetItems.push(timesheetItem2);

      timesheet2.TimeAllocation.push(new TimeAllocation(timesheetItem2, timesheet2.TimesheetDays[0], 3.25, "Travel"));
      timesheet2.TimeAllocation.push(new TimeAllocation(timesheetItem1, timesheet2.TimesheetDays[0], 4.25, "In person meeting"));
      timesheet2.TimeAllocation.push(new TimeAllocation(timesheetItem2, timesheet2.TimesheetDays[1], 3.25, "Travel"));
      timesheet2.TimeAllocation.push(new TimeAllocation(timesheetItem1, timesheet2.TimesheetDays[1], 4.25, "In person meeting"));

      var timesheet3: Timesheet = new Timesheet(3, new Date(2018, 8, 17), new Date(2018, 8, 23));
      timesheet3.Status = eTimesheetStatus.ToDo;

      var timesheet4: Timesheet = new Timesheet(4, new Date(2018, 8, 24), new Date(2018, 8, 30));
      timesheet4.Status = eTimesheetStatus.ToDo;

      this.timesheets.push(timesheet1);
      this.timesheets.push(timesheet2);
      this.timesheets.push(timesheet3);
      this.timesheets.push(timesheet4);
    }

    return this.timesheets;
  }

  getTimesheetById(id: number): Timesheet {
    var timesheet = this.getTimesheets().find(x => x.Id == id);

    return timesheet;
  }

  getTimesheetDay(timesheetId: number, timesheetDayId: number) {
    var timesheet = this.getTimesheets().find(x => x.Id == timesheetId);

    var timesheetDay = timesheet.TimesheetDays.find(x => x.Id == timesheetDayId);

    return timesheetDay;
  }
}